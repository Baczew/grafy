#pragma once

#include <iostream>
#include <iomanip>
#include "S_Lista.h"


template <class Type>
class S_WierzcholekL
{
	//S_Lista<S_KrawedzL<Type>> * wsk_ListaSasiadow;
	Type wartosc;
	int indeks;


public:
	S_WierzcholekL() {}
	S_WierzcholekL(int nowyIndeks) {
		indeks = nowyIndeks;
	}
	S_WierzcholekL(int nowyIndeks, Type nowaWartosc) {
		indeks = nowyIndeks;
		wartosc = nowaWartosc;
	}
	~S_WierzcholekL() {}

	void dodajWartosc(Type nowaWartosc) {
		wartosc = nowaWartosc;
	}
	Type zwrocWartosc()const {
		return wartosc;
	}
	void dodajIndeks(int nowyIndeks) {
		indeks = nowyIndeks;
	}
	int zwrocIndeks()const {
		return indeks;
	}
/*
	void dodajWsk_ListaSasiadow(S_Lista<S_KrawedzL<Type>> * wskaznik) {
		wsk_ListaSasiadow = wskaznik;
	}

	S_Lista<S_KrawedzL<Type>> * zwrocWsk_ListaSasiadow() {
		return wsk_ListaSasiadow;
	}
	*/
};



template <typename Type>
std::ostream& operator << (std::ostream &strumien, const S_WierzcholekL<Type> &wierzcholek) {
	std::cout << wierzcholek.zwrocWartosc();
	return strumien;
}