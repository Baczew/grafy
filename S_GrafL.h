#pragma once

#include <iostream>
#include <iomanip>
#include "S_Kolejka.h"
#include "S_Lista.h"
#include "S_KrawedzL.h"

template <class Type>
class S_GrafL
{

	S_Lista<S_KrawedzL<Type>*> ** listaSasiedztwa;

	S_Lista<S_WierzcholekL<Type>>  listaWierzcholkow;
	S_Lista<S_KrawedzL<Type>>  listaKrawedzi;

	int maxWierzcholkow;
	int maxKrawedzi;
	int wierzcholkow;
	int krawedzi;

public:

	S_GrafL(int wierzcholki, int krawedzie);
	~S_GrafL();

	void wypiszListeSasiedztwa();
	void wypiszListeWierzcholkow();
	void wypiszListeKrawedzi();
	void dodajWierzcholek(Type wartosc);
	void dodajKrawedz(Type waga, int indeks_A, int indeks_B);
	//void dodajPolaczenia(double wypelnienie);
	//double jakaGestosc();
	int algorytmDijkstry(int wierzcholekPoczatkowy, int wierzcholekKoncowy, bool wypisac);
};

template <class Type>
S_GrafL<Type>::S_GrafL(int wierzcholki, int krawedzie)
{
	maxWierzcholkow = wierzcholki;
	maxKrawedzi = wierzcholki * (wierzcholki - 1);

	listaSasiedztwa = new S_Lista<S_KrawedzL<Type>*> *[wierzcholki];

	for (int i = 0; i < wierzcholki; i++) {

		listaSasiedztwa[i] = new S_Lista<S_KrawedzL<Type>*> ;
	}
	

}

template <class Type>
S_GrafL<Type>::~S_GrafL()
{
	for (int i = 0; i < wierzcholkow; i++) {
		delete listaSasiedztwa[i];
	}
	delete listaSasiedztwa;
}


template <class Type>
void S_GrafL<Type>::wypiszListeSasiedztwa() {

	std::cout << "Lista Sasiedctwa:" << endl;
	S_KrawedzL<Type>** pomocnicza;
	for (int i = 0; i < wierzcholkow; i++) {
		int j = 0;
		while (1) {
			pomocnicza = listaSasiedztwa[i]->zwrocWskNaElement(j);
			if (pomocnicza != NULL) std::cout << listaKrawedzi.zwrocIndeksElementu(*pomocnicza) << " ";
			if (pomocnicza == NULL) break;
			j++;
		}
		std::cout << endl;
	}
	if (wierzcholkow == 0) std::cout << "NULL" << std::endl;
	std::cout << endl;
}

template <class Type>
void S_GrafL<Type>::wypiszListeWierzcholkow() {

	std::cout << "Lista Wierzcholkow:" << endl;
	listaWierzcholkow.wypiszElementy();
	std::cout << endl;
}

template <class Type>
void S_GrafL<Type>::wypiszListeKrawedzi() {
	std::cout << "Lista Krawedzi:" << endl;
	listaKrawedzi.wypiszElementy();
	std::cout << endl;
}

template <class Type>
void S_GrafL<Type>::dodajWierzcholek(Type wartosc) {

	listaWierzcholkow.dodajElementNaKoniec(S_WierzcholekL<Type>(wierzcholkow));
	//listaWierzcholkow.zwrocWskNaElement(wierzcholkow)->dodajWsk_ListaSasiadow(listaSasiedztwa[wierzcholkow]);
	wierzcholkow++;
}

template <class Type>
void S_GrafL<Type>::dodajKrawedz(Type waga, int indeks_A, int indeks_B) {
	if (indeks_A < wierzcholkow && indeks_B < wierzcholkow) {

		S_WierzcholekL<Type>* wskIndeks_A = listaWierzcholkow.zwrocWskNaElement(indeks_A);
		S_WierzcholekL<Type>* wskIndeks_B = listaWierzcholkow.zwrocWskNaElement(indeks_B);
		listaKrawedzi.dodajElementNaKoniec(S_KrawedzL<Type>(waga, wskIndeks_A, wskIndeks_B));
		listaSasiedztwa[indeks_A]->dodajElementNaKoniec(listaKrawedzi.zwrocWskNaElementOdKonca(krawedzi));
		krawedzi++;
	}
	else
		std::cout << std::endl << "Polaczenie niemozliwe do zrealizowania!!!" << std::endl;

}



template <class Type>
int S_GrafL<Type>::algorytmDijkstry(int wierzcholekPoczatkowy, int wierzcholekKoncowy, bool wypisac) {

	/*Inicjalizacja: */
	/**********************************************************************************************/
	int* kosztyDojscia = new int[wierzcholkow];
	int* poprzedniki = new int[wierzcholkow];
	int * wierzcholkiPoliczone = new int[wierzcholkow];		//Wierzcho�ki, kt�re przeszli�my
	int wierzcholkiPozostale = wierzcholkow;	//Wierzcho�ki do przej�cia

	for (int i = 0; i < wierzcholkow; i++) {
		wierzcholkiPoliczone[i] = 0;			//0 - Nie przeliczony, 1 - Przeliczony
		kosztyDojscia[i] = 1000000;				//!0;
		poprzedniki[i] = -1;					//Rzaden z wierzcho�k�w na razie nie ma poprzednika 
	}
	kosztyDojscia[wierzcholekPoczatkowy] = 0;	//Koszt doj�cia dla wierzcho�ka startowego 
												/**********************************************************************************************/

												/*Wype�nianie tablic "kosztyDojscia" i "poprzedniki": */
												/**********************************************************************************************/

	while (wierzcholkiPozostale > 0) {

		int indexMin;
		int pomKoszt;
		int poczatek = 0;

		while (poczatek < wierzcholkow) {
			if (wierzcholkiPoliczone[poczatek] == 0) {
				indexMin = poczatek;
				pomKoszt = kosztyDojscia[poczatek];			//Szukamy wierzcho�ka z najmniejszym kosztem doj�cia
				break;
			}
			poczatek++;
		}
		for (int i = poczatek; i < wierzcholkow; i++) {
			if (kosztyDojscia[i] < pomKoszt && wierzcholkiPoliczone[i] == 0) {
				pomKoszt = kosztyDojscia[i];
				indexMin = i;
			}
		}

		wierzcholkiPoliczone[indexMin] = 1;					//Przenosimy ten wierzcho�ek do listy policzonych 
		wierzcholkiPozostale--;								//Zmniejszamy ilo�� wierzcho�k�w do przej�cia

		for (int i = 0; i < listaSasiedztwa[indexMin]->ileJestElementow(); i++) {			//Szukamy s�siad�w tego wierzcho�ka i sprawdzamy koszt przej�cia

			S_KrawedzL<Type> * wsk_krawedz = listaSasiedztwa[indexMin]->zwrocElement(i);
			Type waga = wsk_krawedz->zwrocWage();
			int j = (wsk_krawedz->zwrocWsk_Wierzcholek_B())->zwrocIndeks();

			if (kosztyDojscia[j] > kosztyDojscia[indexMin] + waga) {
				kosztyDojscia[j] = kosztyDojscia[indexMin] + waga;
				poprzedniki[j] = indexMin;
			}
			
		}


	}
	/**********************************************************************************************/

	int koszt = kosztyDojscia[wierzcholekKoncowy];

	if (wypisac == true) {

		std::ofstream plikWynikowy;
		plikWynikowy.open("wynikL.txt");
		plikWynikowy << endl << "Poprzednkiki: ";
		plikWynikowy << "V" << wierzcholekKoncowy;
		int krok = poprzedniki[wierzcholekKoncowy];
		while (krok != -1) {
			plikWynikowy << " --> V" << krok;
			krok = poprzedniki[krok];
		}
		plikWynikowy << endl;

		plikWynikowy << "Koszt: " << koszt << endl;
	}

	/*
	cout << endl << "Poprzednkiki: ";
	for (int i = 0; i < wierzcholkow; i++) {
	cout << poprzedniki[i] << "  ";
	}
	cout << endl;

	cout << "Koszty: ";
	for (int i = 0; i < wierzcholkow; i++) {
	cout << kosztyDojscia[i] << "  ";
	}
	cout << endl;
	*/


	delete[] kosztyDojscia;
	delete[] poprzedniki;
	return koszt;
}
