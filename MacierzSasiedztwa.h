#pragma once

#include <iostream>
#include <iomanip>
#include "S_Kolejka.h"


class MacierzSasiedztwa
{
	int** macierz;

	int wierzcholkow;
	int krawedzi;



public:

	MacierzSasiedztwa(int wierzcholki, int krawedzie);
	~MacierzSasiedztwa();
	void wypisz();
	void dodajPolaczenia(double wypelnienie);
	double jakaGestosc();
	int algorytmDijkstry(int wierzcholekPoczatkowy, int wierzcholekKoncowy);
};


