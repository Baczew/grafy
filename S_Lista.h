#pragma once

#include <iostream>
#include <iomanip>
#include "S_Element.h"

template<class Type>
class S_Lista {

	S_Element<Type> * pierwszy;
	S_Element<Type> * ostatni;
	int iloscElementow = 0;

public:

	S_Lista();
	~S_Lista();
	void dodajElementNaPoczatek(Type element);
	void dodajElementNaKoniec(Type element);
	Type usuńElementZPoczatku();
	Type usuńElementZKonca();
	void wypiszElementy();
	bool czyElementyNalezy(Type szukany);
	int ileJestElementow();
	Type zwrocElement(int index);
	Type* zwrocWskNaElement(int index);
	Type* zwrocWskNaElementOdKonca(int index);
	int zwrocIndeksElementu(Type* szukany);

};


template <class Type>
S_Lista<Type>::S_Lista() {
	pierwszy = NULL;
	ostatni = NULL;
}

template <class Type>
S_Lista<Type>::~S_Lista() {
	S_Element<Type> * pomocniczy1 = NULL;
	pomocniczy1 = pierwszy;
	S_Element<Type> * pomocniczy2 = NULL;
	pomocniczy2 = pierwszy;

	while (pomocniczy1 != NULL) {
		pomocniczy1 = pomocniczy1->zwrócPoprzedni();
		delete pomocniczy2;
		pomocniczy2 = pomocniczy1;

	}
	if (pierwszy == NULL) {
		delete pomocniczy2;
	}
}

template <class Type>
void S_Lista<Type>::dodajElementNaPoczatek(Type element) {
	S_Element<Type> * nowy = new S_Element<Type>;
	nowy->przypiszElement(element);


	if (pierwszy == NULL) {
		pierwszy = nowy;
		ostatni = nowy;
	}
	else {

		pierwszy->przypiszKolejny(nowy);
		nowy->przypiszPoprzedni(pierwszy);
		nowy->przypiszKolejny(NULL);
		pierwszy = nowy;
	}
	iloscElementow++;
}

template <class Type>
void S_Lista<Type>::dodajElementNaKoniec(Type element) {
	S_Element<Type> * nowy = new S_Element<Type>;
	nowy->przypiszElement(element);


	if (ostatni == NULL) {
		pierwszy = nowy;
		ostatni = nowy;
	}
	else {

		ostatni->przypiszPoprzedni(nowy);
		nowy->przypiszKolejny(ostatni);
		nowy->przypiszPoprzedni(NULL);
		ostatni = nowy;
	}
	iloscElementow++;
}

template <class Type>
Type S_Lista<Type>::usuńElementZPoczatku() {
	S_Element<Type> * pomocniczy = NULL;
	Type element;


	if (pierwszy == ostatni) {
		pomocniczy = pierwszy;
		pierwszy = NULL;
		ostatni = NULL;
		iloscElementow--;
	}

	if (pierwszy != NULL) {
		pomocniczy = pierwszy;
		pierwszy = pierwszy->zwrócPoprzedni();
		pierwszy->przypiszKolejny(NULL);
		iloscElementow--;
	}
	else return -1;

	element = pomocniczy->zwrocElement();
	delete pomocniczy;

	return element;
}

template <class Type>
Type S_Lista<Type>::usuńElementZKonca() {
	S_Element<Type> * pomocniczy = NULL;
	Type element;


	if (pierwszy == ostatni) {
		pomocniczy = ostatni;
		pierwszy = NULL;
		ostatni = NULL;
		iloscElementow--;
	}

	if (ostatni != NULL) {
		pomocniczy = ostatni;
		ostatni = ostatni->zwrócKolejny();
		ostatni->przypiszpoprzedni(NULL);
		iloscElementow--;
	}
	else return NULL;

	element = pomocniczy->zwrocElement();
	delete pomocniczy;

	return element;
}

template <class Type>
void S_Lista<Type>::wypiszElementy() {
	S_Element<Type> * pomocniczy = NULL;
	pomocniczy = pierwszy;

	while (pomocniczy != NULL) {
		std::cout << pomocniczy->zwrocElement() << "  ";
		pomocniczy = pomocniczy->zwrócPoprzedni();
	}
	if (pierwszy == NULL) {
		std::cout << "NULL  ";
	}

}

template <class Type>																//<--------------!!!!!!!!!!!!!!!!!
bool S_Lista<Type>::czyElementyNalezy(Type szukany) {
	S_Element<Type> * pomocniczy = NULL;
	pomocniczy = pierwszy;

	while (pomocniczy != NULL) {
		if (pomocniczy->zwrocElement() == szukany) return 1;
		pomocniczy = pomocniczy->zwrócPoprzedni();
	}
	return 0;

}


template <class Type>
int S_Lista<Type>::ileJestElementow() {
	return iloscElementow;
}

template <class Type>																//<--------------!!!!!!!!!!!!!!!!!
Type S_Lista<Type>::zwrocElement(int index){
	index = index+1;
	S_Element<Type> * pomocniczy = NULL;
	int podzial = iloscElementow / 2;

	if (index <= iloscElementow) {
		if (index <= podzial) {
			pomocniczy = pierwszy;
			int pom = 1;
			while (pom != index) {
				pomocniczy = pomocniczy->zwrócPoprzedni();
				pom++;
			}
			return pomocniczy->zwrocElement();
		}
		else if (index > podzial) {
			int ile = iloscElementow - (index);
			pomocniczy = ostatni;
			int pom = 0;
			while (pom != ile) {
				pomocniczy = pomocniczy->zwrocKolejny();
				pom++;
			}
			return pomocniczy->zwrocElement();
		}
	}
	return NULL;
}

template <class Type>																//<--------------!!!!!!!!!!!!!!!!!
Type* S_Lista<Type>::zwrocWskNaElement(int index) {
	 index++;
	S_Element<Type> * pomocniczy = NULL;
	pomocniczy = pierwszy;
	int pom = 1;

	if (index <= iloscElementow) {
		while (pom != index) {
			pomocniczy = pomocniczy->zwrócPoprzedni();
			pom++;
		}
		return pomocniczy->zwrocWskNaElement();
	}
	return NULL;

}
template <class Type>																//<--------------!!!!!!!!!!!!!!!!!
Type* S_Lista<Type>::zwrocWskNaElementOdKonca(int index) {
	int ile =iloscElementow - (index + 1);
	
	S_Element<Type> * pomocniczy = NULL;
	pomocniczy = ostatni;
	int pom = 0;

	if (index < iloscElementow) {
		while (pom != ile) {
			pomocniczy = pomocniczy->zwrocKolejny();
			pom++;
		}
		return pomocniczy->zwrocWskNaElement();
	}
	return NULL;

}

template <class Type>
int S_Lista<Type>::zwrocIndeksElementu( Type* szukany) {
	S_Element<Type> * pomocniczy = NULL;
	pomocniczy = pierwszy;
	int pom = 0;

	while (pomocniczy != NULL) {
		Type * wsk = pomocniczy->zwrocWskNaElement();
		if (wsk == szukany) {
			return pom;
		}
		pomocniczy = pomocniczy->zwrócPoprzedni();
		pom++;
	}
	return -1;

}