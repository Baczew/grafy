// Grafy.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

// Macierz sąsiedztwa
// Data: 14.07.2013
// (C)2013 mgr Jerzy Wałaszek
//---------------------------

#include <iostream>
#include <iomanip>
#include "MacierzSasiedztwa.h"
#include "ListaSasiedztwa.h"
#include "S_Kolejka.h"
#include "S_Lista.h"

using namespace std;

int main()
{
	srand(time(NULL));
	/*
	
	S_Lista<int> lista;

	for (int i = 0; i < 10; i++) {
		lista.dodajElement(i);
	}
	lista.wypiszElementy();

	cout << endl << "Zwroc element 1: " << lista.zwrocElement(1) << endl;
	cout << endl << "Ile: " << lista.ileJestElementow() << endl;

	for (int i = 0; i < 10; i++) {
		lista.usuńElement();
		cout << endl;
		lista.wypiszElementy();
	}
	cout << endl << "Zwroc element 1: " << lista.zwrocElement(1) << endl;
	cout << endl << "Ile: " << lista.ileJestElementow() << endl;

	

	*/

	int w = 10;
	int k;
	k = (w*(w - 1)) / 2;
	int wypelnienie = 1;

	ListaSasiedztwa Graf1(w, k);
	Graf1.dodajPolaczenia(0.5);
	Graf1.wypisz();

	cout << "Koszt dojscia z V" << 3 << " do V" << 9 << " to:  " << Graf1.algorytmDijkstry(3, 9);

	cout << endl << endl;

	MacierzSasiedztwa Graf(w, k);
	Graf.wypisz();
	Graf.dodajPolaczenia(0.5);
	Graf.wypisz();

	cout << "Gestosc: " << Graf.jakaGestosc() << endl;
	cout << "Koszt dojscia z V" << 3 << " do V" << 9 << " to:  " << Graf.algorytmDijkstry(3, 9);
	

	cout << endl << "KONIEC" << endl;
	while (1);
	return 0;
	
}