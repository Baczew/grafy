#pragma once

#include <iostream>
#include <iomanip>
#include "S_Kolejka.h"
#include "S_Lista.h"


class ListaSasiedztwa
{
	S_Lista<int> * tablicaWierzcholkow;

	int wierzcholkow;
	int krawedzi;



public:

	ListaSasiedztwa(int wierzcholki, int krawedzie);
	~ListaSasiedztwa();
	void wypisz();
	void dodajPolaczenia(double wypelnienie);
	//double jakaGestosc();
	int algorytmDijkstry(int wierzcholekPoczatkowy, int wierzcholekKoncowy);
};


