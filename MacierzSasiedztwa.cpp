#include "stdafx.h"
#include "MacierzSasiedztwa.h"

using namespace std;

MacierzSasiedztwa::MacierzSasiedztwa(int wierzcholki, int krawedzie)
{
	wierzcholkow = wierzcholki;
	krawedzi = krawedzie;

	macierz = new int *[wierzcholki];

	for (int i = 0; i < wierzcholki; i++) {
		macierz[i] = new int[wierzcholki];
	}
	for (int i = 0; i < wierzcholki; i++) {
		for (int j = 0; j < wierzcholki; j++) {
			macierz[i][j] = 0;
		}
	}

}


MacierzSasiedztwa::~MacierzSasiedztwa()
{
	for (int i = 0; i < wierzcholkow; i++) {
		delete[] macierz[i];
	}
	delete macierz;
}


void MacierzSasiedztwa::wypisz(){

	cout << "Macierz Sasiedctwa:" << endl;
	for (int i = 0; i < wierzcholkow; i++) {
		for (int j = 0; j < wierzcholkow; j++) {
			cout << macierz[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;

}

void MacierzSasiedztwa::dodajPolaczenia(double wypelnienie) {
	int maxPolaczen =  wypelnienie * (wierzcholkow - 1);
	int sumaPolonczen = 0;
	int sumaWiersza = 0;

	for (int i = 0; i < wierzcholkow; i++) {
		for (int j = 0; j < wierzcholkow; j++)
			if (macierz[i][j] == 1)
				sumaPolonczen++;

		while (sumaPolonczen < maxPolaczen) {
			int pom = rand() % wierzcholkow;

			if (macierz[pom][i] == 0 && macierz[i][pom] == 0 && i != pom && sumaWiersza < maxPolaczen) {
				macierz[i][pom] = 1;
				int suma = 0;
				for (int j = 0; j < wierzcholkow; j++)
					if (macierz[pom][j] == 1)
						suma++;
				if (suma < maxPolaczen) 
					macierz[pom][i] = macierz[i][pom];
				sumaPolonczen++;
			}
		}
		sumaPolonczen= 0;
	}

	for (int i = 0; i < wierzcholkow; i++) {
		for (int j = i+1; j < wierzcholkow; j++) {
			if (macierz[i][j] != 0) {
				macierz[i][j] = rand()%9 +1;
				macierz[j][i] = macierz[i][j];
			}
				
		}
	}


/*
	for (int i = 0; i < wierzcholkow; i++) {
		while (sumaPolonczen < maxPolaczen) {
			for (int j = i+1; j < wierzcholkow; j++) {
				if (macierz[i][j] == 0) {
					int pomocnicza = rand() % 2;
					if (sumaPolonczen < maxPolaczen && pomocnicza == 1) {
						macierz[i][j] = pomocnicza;
						sumaPolonczen++;
					}
				}

			}
		}
		sumaPolonczen = 0;
		maxPolaczen = wypelnienie * (wierzcholkow - i- 1);
	}

	for (int i = 1; i < wierzcholkow; i++)
		for (int j = 0; j < i; j++)
			macierz[i][j] = macierz[j][i];
			*/
}


double MacierzSasiedztwa::jakaGestosc() {
	double sumaJedynek = 0;
	for (int i = 0; i < wierzcholkow; i++)
		for (int j = 0; j < wierzcholkow; j++)
			if(macierz[i][j]>0)
				sumaJedynek++;

	return	sumaJedynek / (wierzcholkow * wierzcholkow - wierzcholkow);
}



int MacierzSasiedztwa::algorytmDijkstry(int wierzcholekPoczatkowy, int wierzcholekKoncowy) {
	
	/*Inicjalizacja: */
	/**********************************************************************************************/
	int* kosztyDojscia = new int[wierzcholkow];
	int* poprzedniki = new int[wierzcholkow];
	int * wierzcholkiPoliczone = new int[wierzcholkow];		//Wierzcho�ki, kt�re przeszli�my
	int wierzcholkiPozostale = wierzcholkow;	//Wierzcho�ki do przej�cia

	for (int i = 0; i < wierzcholkow; i++) {
		wierzcholkiPoliczone[i] = 0;			//0 - Nie przeliczony, 1 - Przeliczony
		kosztyDojscia[i] = 1000000;				//!0;
		poprzedniki[i] = -1;					//Rzaden z wierzcho�k�w na razie nie ma poprzednika 
	}
	kosztyDojscia[wierzcholekPoczatkowy] = 0;	//Koszt doj�cia dla wierzcho�ka startowego 
	/**********************************************************************************************/

	/*Wype�nianie tablic "kosztyDojscia" i "poprzedniki": */
	/**********************************************************************************************/
	
	while (wierzcholkiPozostale > 0) {

		int indexMin;
		int pomKoszt;
		int poczatek=0;
		while (poczatek < wierzcholkow) {
			if (wierzcholkiPoliczone[poczatek] == 0) {
				indexMin = poczatek;
				pomKoszt = kosztyDojscia[poczatek];			//Szukamy wierzcho�ka z najmniejszym kosztem doj�cia
				break;
			}
			poczatek++;
		}
		for (int i = poczatek; i < wierzcholkow; i++) {
			if (kosztyDojscia[i] < pomKoszt && wierzcholkiPoliczone[i] == 0) {
				pomKoszt = kosztyDojscia[i];
				indexMin = i;
			}
		}

		wierzcholkiPoliczone[indexMin] = 1;					//Przenosimy ten wierzcho�ek do listy policzonych 
		wierzcholkiPozostale--;								//Zmniejszamy ilo�� wierzcho�k�w do przej�cia
		for (int i = 0; i < wierzcholkow; i++) {			//Szukamy s�siad�w tego wierzcho�ka i sprawdzamy koszt przej�cia
			if (macierz[indexMin][i] > 0) {
				if (kosztyDojscia[i] > kosztyDojscia[indexMin] + macierz[indexMin][i]) {
					kosztyDojscia[i] = kosztyDojscia[indexMin] + macierz[indexMin][i];
					poprzedniki[i] = indexMin;
				}
			}
		}

	}
	/**********************************************************************************************/
	
	cout << endl << "Poprzednkiki: ";
	cout << "V" << wierzcholekKoncowy;
	int krok = poprzedniki[wierzcholekKoncowy];
	while (krok != -1) {
			cout << " --> V" << krok;
			krok = poprzedniki[krok];
	}
	cout << endl;

	/*
	cout << endl << "Poprzednkiki: ";
	for (int i = 0; i < wierzcholkow; i++) {
		cout << poprzedniki[i] << "  ";
	}
	cout << endl;
	
	cout << "Koszty: ";
	for (int i = 0; i < wierzcholkow; i++) {
		cout << kosztyDojscia[i] << "  ";
	}
	cout << endl;
	*/



	int koszt = kosztyDojscia[wierzcholekKoncowy];
	delete[] kosztyDojscia;
	delete [] poprzedniki;
	return koszt;
}