#pragma once

#include <iostream>
#include <iomanip>
#include "S_Lista.h"
#include "S_WierzcholekM.h"

template <class Type>
class S_KrawedzM
{
	Type waga;
	S_WierzcholekM<Type> * wsk_wierzcholek_A;
	S_WierzcholekM<Type> * wsk_wierzcholek_B;
	

public:
	S_KrawedzM() {}
	S_KrawedzM(S_WierzcholekM<Type> * nowyWsk_wierzcholek_A, S_WierzcholekM<Type> * nowyWsk_wierzcholek_B) {
		wsk_wierzcholek_A = nowyWsk_wierzcholek_A;
		wsk_wierzcholek_B = nowyWsk_wierzcholek_B;
		waga = 1;
	}
	S_KrawedzM(Type nowaWaga,S_WierzcholekM<Type> * nowyWsk_wierzcholek_A, S_WierzcholekM<Type> * nowyWsk_wierzcholek_B) {
		wsk_wierzcholek_A = nowyWsk_wierzcholek_A;
		wsk_wierzcholek_B = nowyWsk_wierzcholek_B;
		waga = nowaWaga;
	}
	~S_KrawedzM() {}

	void dodajWage(Type nowaWaga) {
		waga = nowaWaga;
	}
	Type zwrocWage()const {
		return waga;
	}
	S_WierzcholekM<Type> * zwrocWsk_Wierzcholek_A()const {
		return wsk_wierzcholek_A;
	}
	S_WierzcholekM<Type> * zwrocWsk_Wierzcholek_B()const {
		return wsk_wierzcholek_B;
	}
};


template <typename Type>
std::ostream& operator << (std::ostream &strumien, const S_KrawedzM<Type> &krawedz) {
	std::cout << krawedz.zwrocWage();
	return strumien;
}