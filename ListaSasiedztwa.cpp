#include "stdafx.h"
#include "ListaSasiedztwa.h"


using namespace std;

ListaSasiedztwa::ListaSasiedztwa(int wierzcholki, int krawedzie)
{
	tablicaWierzcholkow = new S_Lista<int> [wierzcholki];
	wierzcholkow = wierzcholki;
	krawedzi = krawedzie;

}


ListaSasiedztwa::~ListaSasiedztwa()
{
	delete [] tablicaWierzcholkow;
}

void ListaSasiedztwa::wypisz() {

	cout << "Lista Sasiedztwa:" << endl;
	for (int i = 0; i < wierzcholkow; i++) {
		tablicaWierzcholkow[i].wypiszElementy();
		cout << endl;
		}
		cout << endl;

}

void ListaSasiedztwa::dodajPolaczenia(double wypelnienie) {
	int maxPolaczen = wypelnienie * (wierzcholkow - 1);
	int sumaPolonczen = 0;
	int sumaWiersza = 0;

	for (int i = 0; i < wierzcholkow; i++) {
		sumaPolonczen = tablicaWierzcholkow[i].ileJestElementow();

		while (sumaPolonczen < maxPolaczen) {
			int pom = rand() % wierzcholkow;
			if (tablicaWierzcholkow[i].czyElementyNalezy(pom) == 0) {
				if (pom != i) {
					tablicaWierzcholkow[i].dodajElement(pom);
					if(tablicaWierzcholkow[pom].ileJestElementow() < maxPolaczen)
						tablicaWierzcholkow[pom].dodajElement(i);
					sumaPolonczen++;
				}
			}
		}
		sumaPolonczen = 0;
	}
}

int ListaSasiedztwa::algorytmDijkstry(int wierzcholekPoczatkowy, int wierzcholekKoncowy) {

	/*Inicjalizacja: */
	/**********************************************************************************************/
	int* kosztyDojscia = new int[wierzcholkow];
	int* poprzedniki = new int[wierzcholkow];
	int * wierzcholkiPoliczone = new int[wierzcholkow];		//Wierzcho�ki, kt�re przeszli�my
	int wierzcholkiPozostale = wierzcholkow;	//Wierzcho�ki do przej�cia

	for (int i = 0; i < wierzcholkow; i++) {
		wierzcholkiPoliczone[i] = 0;			//0 - Nie przeliczony, 1 - Przeliczony
		kosztyDojscia[i] = 1000000;				//!0;
		poprzedniki[i] = -1;					//Rzaden z wierzcho�k�w na razie nie ma poprzednika 
	}
	kosztyDojscia[wierzcholekPoczatkowy] = 0;	//Koszt doj�cia dla wierzcho�ka startowego 
												/**********************************************************************************************/

												/*Wype�nianie tablic "kosztyDojscia" i "poprzedniki": */
												/**********************************************************************************************/

	while (wierzcholkiPozostale > 0) {

		int indexMin;
		int pomKoszt;
		int poczatek = 0;

		while (poczatek < wierzcholkow) {
			if (wierzcholkiPoliczone[poczatek] == 0) {
				indexMin = poczatek;
				pomKoszt = kosztyDojscia[poczatek];			//Szukamy wierzcho�ka z najmniejszym kosztem doj�cia
				break;
			}
			poczatek++;
		}
		for (int i = poczatek; i < wierzcholkow; i++) {
			if (kosztyDojscia[i] < pomKoszt && wierzcholkiPoliczone[i] == 0) {
				pomKoszt = kosztyDojscia[i];
				indexMin = i;
			}
		}

		wierzcholkiPoliczone[indexMin] = 1;					//Przenosimy ten wierzcho�ek do listy policzonych 
		wierzcholkiPozostale--;								//Zmniejszamy ilo�� wierzcho�k�w do przej�cia

		for (int i = 1; i <= tablicaWierzcholkow[indexMin].ileJestElementow(); i++) {			//Szukamy s�siad�w tego wierzcho�ka i sprawdzamy koszt przej�cia
	
				if (kosztyDojscia[tablicaWierzcholkow[indexMin].zwrocElement(i)] > kosztyDojscia[indexMin] + 1 ){
					kosztyDojscia[tablicaWierzcholkow[indexMin].zwrocElement(i)] = kosztyDojscia[indexMin] + 1;
					poprzedniki[tablicaWierzcholkow[indexMin].zwrocElement(i)] = indexMin;
				}
		}
	

	}
	/**********************************************************************************************/

	cout << endl << "Poprzednkiki: ";
	cout << "V" << wierzcholekKoncowy;
	int krok = poprzedniki[wierzcholekKoncowy];
	while (krok != -1) {
		cout << " --> V" << krok;
		krok = poprzedniki[krok];
	}
	cout << endl;
	
	/*
	cout << endl << "Poprzednkiki: ";
	for (int i = 0; i < wierzcholkow; i++) {
	cout << poprzedniki[i] << "  ";
	}
	cout << endl;

	cout << "Koszty: ";
	for (int i = 0; i < wierzcholkow; i++) {
	cout << kosztyDojscia[i] << "  ";
	}
	cout << endl;
	*/



	int koszt = kosztyDojscia[wierzcholekKoncowy];
	delete[] kosztyDojscia;
	delete[] poprzedniki;
	return koszt;
}
