#pragma once

#include <iostream>
#include <iomanip>
#include "S_Kolejka.h"
#include "S_Lista.h"
#include "S_KrawedzM.h"

template <class Type>
class S_GrafM
{
	S_KrawedzM<Type>*** macierz;
	
	S_Lista<S_WierzcholekM<Type>>  listaWierzcholkow;
	S_Lista<S_KrawedzM<Type>>  listaKrawedzi;

	int maxWierzcholkow;
	int maxKrawedzi;
	int wierzcholkow;
	int krawedzi;

public:

	S_GrafM(int wierzcholki, int krawedzie);
	~S_GrafM();
	void wypiszMacierz();
	void wypiszListeWierzcholkow();
	void wypiszListeKrawedzi();
	void dodajWierzcholek(Type wartosc);
	void dodajKrawedz(Type waga,  int indeks_A, int indeks_B);
	//void dodajPolaczenia(double wypelnienie);
	//double jakaGestosc();
	int algorytmDijkstry(int wierzcholekPoczatkowy, int wierzcholekKoncowy, bool wypisac);
};


template <class Type>
S_GrafM<Type>::S_GrafM(int wierzcholki, int krawedzie)
{
	maxWierzcholkow = wierzcholki;
	maxKrawedzi = wierzcholki * (wierzcholki - 1);

	macierz = new S_KrawedzM<Type> **[wierzcholki];

	for (int i = 0; i < wierzcholki; i++) {
		macierz[i] = new S_KrawedzM<Type>*[wierzcholki];
	}
	for (int i = 0; i < wierzcholki; i++) {
		for (int j = 0; j < wierzcholki; j++) {
			macierz[i][j] = NULL;
		}
	}

}

template <class Type>
S_GrafM<Type>::~S_GrafM()
{
	for (int i = 0; i < wierzcholkow; i++) {
		delete[] macierz[i];
	}
	delete macierz;
}

template <class Type>
void S_GrafM<Type>::wypiszMacierz() {

	std::cout << "Macierz Sasiedctwa:" << endl;
	for (int i = 0; i < wierzcholkow; i++) {
		for (int j = 0; j < wierzcholkow; j++) {
			if (macierz[i][j] == NULL)
				std::cout << "N" << " ";
			else
				std::cout << listaKrawedzi.zwrocIndeksElementu(macierz[i][j]) << " ";;
		}
		std::cout << endl;
	}
	std::cout << endl;

}

template <class Type>
void S_GrafM<Type>::wypiszListeWierzcholkow() {

	std::cout << "Lista Wierzcholkow:" << endl;
	listaWierzcholkow.wypiszElementy();
	std::cout << endl;
}

template <class Type>
void S_GrafM<Type>::wypiszListeKrawedzi() {
	std::cout << "Lista Krawedzi:" << endl;
	listaKrawedzi.wypiszElementy();
	std::cout << endl;
}

template <class Type>
void S_GrafM<Type>::dodajWierzcholek(Type wartosc) {
	
	if (wierzcholkow < maxWierzcholkow) {
		listaWierzcholkow.dodajElementNaKoniec( S_WierzcholekM<Type> (wierzcholkow, wartosc));
		wierzcholkow++;
	}
	else {

		maxWierzcholkow = maxWierzcholkow * 2;
		S_KrawedzM<Type>*** nowaMacierz = new S_KrawedzM<Type> **[maxWierzcholkow];

		for (int i = 0; i < maxWierzcholkow; i++) {
			nowaMacierz[i] = new S_KrawedzM<Type>*[maxWierzcholkow];
			for (int j = 0; j < maxWierzcholkow; j++) {
				nowaMacierz[i][j] = NULL;
			}
		}

		for (int i = 0; i < maxWierzcholkow / 2; i++) {
			for (int j = 0; j < maxWierzcholkow / 2; j++) {
				if (macierz[i][j] != NULL)
					nowaMacierz[i][j] = macierz[i][j];
			}
		}
		
		for (int i = 0; i < wierzcholkow; i++) {
			delete[] macierz[i];
		}
		delete macierz;

		macierz = nowaMacierz;

		listaWierzcholkow.dodajElementNaKoniec(S_WierzcholekM<Type>(wierzcholkow, wartosc));
		wierzcholkow++;
	}
		
}



template <class Type>
void S_GrafM<Type>::dodajKrawedz(Type waga, int indeks_A, int indeks_B) {
	if (indeks_A < wierzcholkow && indeks_B < wierzcholkow) {

		S_WierzcholekM<Type>* wskIndeks_A = listaWierzcholkow.zwrocWskNaElement(indeks_A);
		S_WierzcholekM<Type>* wskIndeks_B = listaWierzcholkow.zwrocWskNaElement(indeks_B);
		listaKrawedzi.dodajElementNaKoniec(S_KrawedzM<Type>(waga, wskIndeks_A, wskIndeks_B));
		
		macierz[indeks_A][indeks_B] = listaKrawedzi.zwrocWskNaElementOdKonca(krawedzi);
		krawedzi++;

		/*																			//<-----Z listy kraw�dzi doj�cie do wierzcho�ka 
		S_Krawedz<Type> * krawedz = listaKrawedzi.zwrocWskNaElement(0);
		S_Wierzcholek<Type> * wierzcholek = krawedz->zwrocWsk_Wierzcholek_A();
		wierzcholek->dodajWartosc(-2);
		*/
	}
	else
		std::cout << std::endl << "Polaczenie niemozliwe do zrealizowania!!!" << std::endl;

}


template <class Type>
int S_GrafM<Type>::algorytmDijkstry(int wierzcholekPoczatkowy, int wierzcholekKoncowy, bool wypisac) {

	/*Inicjalizacja: */
	/**********************************************************************************************/
	int* kosztyDojscia = new int[wierzcholkow];
	int* poprzedniki = new int[wierzcholkow];
	int * wierzcholkiPoliczone = new int[wierzcholkow];		//Wierzchoki, ktre przeszlimy
	int wierzcholkiPozostale = wierzcholkow;	//Wierzchoki do przejcia

	for (int i = 0; i < wierzcholkow; i++) {
		wierzcholkiPoliczone[i] = 0;			//0 - Nie przeliczony, 1 - Przeliczony
		kosztyDojscia[i] = 1000000;				//!0;
		poprzedniki[i] = -1;					//Rzaden z wierzchokw na razie nie ma poprzednika 
	}
	kosztyDojscia[wierzcholekPoczatkowy] = 0;	//Koszt dojcia dla wierzchoka startowego 
												/**********************************************************************************************/

												/*Wypenianie tablic "kosztyDojscia" i "poprzedniki": */
												/**********************************************************************************************/

	while (wierzcholkiPozostale > 0) {

		int indexMin;
		int pomKoszt;
		int poczatek = 0;
		while (poczatek < wierzcholkow) {
			if (wierzcholkiPoliczone[poczatek] == 0) {
				indexMin = poczatek;
				pomKoszt = kosztyDojscia[poczatek];			//Szukamy wierzchoka z najmniejszym kosztem dojcia
				break;
			}
			poczatek++;
		}
		for (int i = poczatek; i < wierzcholkow; i++) {
			if (kosztyDojscia[i] < pomKoszt && wierzcholkiPoliczone[i] == 0) {
				pomKoszt = kosztyDojscia[i];
				indexMin = i;
			}
		}

		wierzcholkiPoliczone[indexMin] = 1;					//Przenosimy ten wierzchoek do listy policzonych 
		wierzcholkiPozostale--;								//Zmniejszamy ilo wierzchokw do przejcia
		for (int i = 0; i < wierzcholkow; i++) {			//Szukamy ssiadw tego wierzchoka i sprawdzamy koszt przejcia
			if (macierz[indexMin][i] > 0) {
				S_KrawedzM<Type> * wsk_krawedz = macierz[indexMin][i];
				Type waga = wsk_krawedz->zwrocWage();
				if (kosztyDojscia[i] > kosztyDojscia[indexMin] + waga) {
					kosztyDojscia[i] = kosztyDojscia[indexMin] + waga;
					poprzedniki[i] = indexMin;
				}
			}
		}

	}
	/**********************************************************************************************/

	int koszt = kosztyDojscia[wierzcholekKoncowy];

	if (wypisac == true) {

		std::ofstream plikWynikowy;
		plikWynikowy.open("wynikM.txt");
		plikWynikowy << endl << "Poprzednkiki: ";
		plikWynikowy << "V" << wierzcholekKoncowy;
		int krok = poprzedniki[wierzcholekKoncowy];
		while (krok != -1) {
			plikWynikowy << " --> V" << krok;
			krok = poprzedniki[krok];
		}
		plikWynikowy << endl;

		plikWynikowy << "Koszt: " << koszt << endl;
	}
	/*
	cout << endl << "Poprzednkiki: ";
	for (int i = 0; i < wierzcholkow; i++) {
	cout << poprzedniki[i] << "  ";
	}
	cout << endl;

	cout << "Koszty: ";
	for (int i = 0; i < wierzcholkow; i++) {
	cout << kosztyDojscia[i] << "  ";
	}
	cout << endl;
	*/


	delete[] kosztyDojscia;
	delete[] poprzedniki;
	return koszt;
}
