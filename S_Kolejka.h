#pragma once


#include <iostream>
#include <iomanip>
#include "S_Element.h"

template<class Type>
class S_Kolejka {

	S_Element<Type> * pierwszy;
	S_Element<Type> * ostatni;

public:

	S_Kolejka();
	~S_Kolejka();
	void dodajElement(Type element);
	Type usu�Element();
	void wypiszElementy();

};


template <class Type>
S_Kolejka<Type>::S_Kolejka() {
	pierwszy = NULL;
	ostatni = NULL;
}

template <class Type>
S_Kolejka<Type>::~S_Kolejka() {

}

template <class Type>
void S_Kolejka<Type>::dodajElement(Type element) {
	S_Element<Type> * nowy = new S_Element<Type>;
	nowy->przypiszElement(element);

	if (ostatni == NULL){
		pierwszy = nowy;
		ostatni = nowy;
	}
	else {
		

		ostatni->przypiszPoprzedni(nowy);
		nowy->przypiszKolejny(ostatni);
		nowy->przypiszPoprzedni(NULL);
		ostatni = nowy;
	}

}

template <class Type>
Type S_Kolejka<Type>::usu�Element() {
	S_Element<Type> * pomocniczy = NULL;
	Type element;


	if (pierwszy == ostatni) {
		pomocniczy = pierwszy;
		pierwszy = NULL;
		ostatni = NULL;
	}

	if (pierwszy != NULL) {
		pomocniczy = pierwszy;
		pierwszy = pierwszy->zwr�cPoprzedni();
		pierwszy->przypiszKolejny(NULL);
	}
	else return -1;

	element = pomocniczy->zwrocElement();
	delete pomocniczy;

	return element;
}

template <class Type>
void S_Kolejka<Type>::wypiszElementy() {
	S_Element<Type> * pomocniczy = NULL;
	pomocniczy = pierwszy;

	while (pomocniczy != NULL) {
		std::cout << pomocniczy->zwrocElement() << "  ";
		pomocniczy = pomocniczy->zwr�cPoprzedni();
	}
}