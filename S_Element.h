#pragma once

#include <iostream>
#include <iomanip>

template<class Type>
class S_Element {
	Type element;
	S_Element<Type> * kolejny;
	S_Element<Type> * poprzedni;

public:
	S_Element();
	~S_Element();
	void przypiszElement(Type elem);
	void przypiszKolejny(S_Element<Type>* wsk_Kolejny);
	void przypiszPoprzedni(S_Element<Type>* wsk_Poprzedni);
	Type zwrocElement()const;
	Type* zwrocWskNaElement();
	S_Element<Type>* zwrocKolejny()const;
	S_Element<Type>* zwrócPoprzedni()const;

};

template <class Type>
S_Element<Type>::S_Element() {
	kolejny = NULL;
	poprzedni = NULL;
}

template <class Type>
S_Element<Type>::~S_Element() {

}

template<class Type>
void S_Element<Type>::przypiszElement(Type elem) {
	element = elem;
}

template<class Type>
void S_Element<Type>::przypiszKolejny(S_Element<Type>* wsk_Kolejny){
	kolejny = wsk_Kolejny;
}


template<class Type>
void S_Element<Type>::przypiszPoprzedni(S_Element<Type>* wsk_Poprzedni){
	poprzedni = wsk_Poprzedni;
}

template<class Type>
Type S_Element<Type>::zwrocElement()const {
	return element;
}

template<class Type>
 Type* S_Element<Type>::zwrocWskNaElement() {
	return &element;
}


template<class Type>
S_Element<Type>* S_Element<Type>::zwrocKolejny()const{
	return kolejny;
}


template<class Type>
S_Element<Type>* S_Element<Type>::zwrócPoprzedni()const {
	return poprzedni;
}


template <typename Type>
std::ostream& operator << (std::ostream &strumien, const S_Element<Type> &elem) { 
	std::cout << elem.zwrocElement();
	return strumien;
}
