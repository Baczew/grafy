#pragma once

#include <iostream>
#include <iomanip>
#include "S_Lista.h"

template <class Type>
class S_WierzcholekM
{
	Type wartosc;
	int indeks;


public:
	S_WierzcholekM() {}
	S_WierzcholekM(int nowyIndeks) {
		indeks = nowyIndeks;
	}
	S_WierzcholekM(int nowyIndeks, Type nowaWartosc) {
		indeks = nowyIndeks;
		wartosc = nowaWartosc;
	}
	~S_WierzcholekM(){}

	void dodajWartosc(Type nowaWartosc) {
		wartosc = nowaWartosc;
	}
	Type zwrocWartosc()const {
		return wartosc;
	}
	void dodajIndeks(int nowyIndeks) {
		indeks = nowyIndeks;
	}
	int zwrocIndeks()const {
		return indeks;
	}


};



template <typename Type>
std::ostream& operator << (std::ostream &strumien, const S_WierzcholekM<Type> &wierzcholek) {
	std::cout << wierzcholek.zwrocWartosc();
	return strumien;
}