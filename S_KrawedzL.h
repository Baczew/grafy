#pragma once

#include <iostream>
#include <iomanip>
#include "S_Lista.h"
#include "S_WierzcholekL.h"

template <class Type>
class S_KrawedzL
{
	Type waga;
	S_WierzcholekL<Type> * wsk_wierzcholek_A;
	S_WierzcholekL<Type> * wsk_wierzcholek_B;


public:
	S_KrawedzL() {}
	S_KrawedzL(S_WierzcholekL<Type> * nowyWsk_wierzcholek_A, S_WierzcholekL<Type> * nowyWsk_wierzcholek_B) {
		wsk_wierzcholek_A = nowyWsk_wierzcholek_A;
		wsk_wierzcholek_B = nowyWsk_wierzcholek_B;
		waga = 1;
	}
	S_KrawedzL(Type nowaWaga, S_WierzcholekL<Type> * nowyWsk_wierzcholek_A, S_WierzcholekL<Type> * nowyWsk_wierzcholek_B) {
		wsk_wierzcholek_A = nowyWsk_wierzcholek_A;
		wsk_wierzcholek_B = nowyWsk_wierzcholek_B;
		waga = nowaWaga;
	}
	~S_KrawedzL() {}

	void dodajWage(Type nowaWaga) {
		waga = nowaWaga;
	}
	Type zwrocWage()const {
		return waga;
	}
	S_WierzcholekL<Type> * zwrocWsk_Wierzcholek_A()const {
		return wsk_wierzcholek_A;
	}
	S_WierzcholekL<Type> * zwrocWsk_Wierzcholek_B()const {
		return wsk_wierzcholek_B;
	}
};


template <typename Type>
std::ostream& operator << (std::ostream &strumien, const S_KrawedzL<Type> &krawedz) {
	std::cout << krawedz.zwrocWage();
	return strumien;
}